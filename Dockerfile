FROM node:alpine
WORKDIR /app
COPY ./build/ .
CMD ["sh", "-c", "npx -y serve -s /app"]
