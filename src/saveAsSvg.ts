import FileSaver from 'file-saver'

export default function saveAsSvg(svgText: string) {
  const blob = new Blob([svgText], { type: 'text/plain;charset=utf-8' })
  FileSaver.saveAs(blob, 'qr-code.svg')
}
