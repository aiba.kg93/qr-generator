import QR from 'qrcode-generator'
import type {
  ChangeEventHandler,
  Dispatch,
  FormEventHandler,
  LegacyRef,
  ReactNode,
  SetStateAction,
} from 'react'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import type { InputProps } from 'reactstrap'
import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap'
import saveAsJson from './saveAsJson'
import saveAsSvg from './saveAsSvg'

type NumericParams = {
  start: number | string
  count: number | string
  width: number | string
  padding: number | string
  countPerRow: number | string
  copies: number | string
  topLabelSize: number | string
  topLabelHeight: number | string
  bottomLabelSize: number | string
  bottomLabelHeight: number | string
}

type StringParams = {
  prefix: string
  topLabelColor: string
  topLabelBackgroundColor: string
  bottomLabelColor: string
  bottomLabelBackgroundColor: string
}

type Params = NumericParams & StringParams

type BaseInputProps = {
  label: ReactNode
  name: keyof Params
  model: Params
  setModel: Dispatch<SetStateAction<Params>>
} & InputProps

function BaseInput({ label, name, model, setModel, ...props }: BaseInputProps) {
  const value = useMemo(() => {
    return model[name] || ''
  }, [model, name])
  const onChange: ChangeEventHandler<HTMLInputElement> = useCallback(
    e => {
      const { value } = e.target
      setModel(model => ({ ...model, [name]: value.toUpperCase() }))
    },
    [name, setModel],
  )
  return (
    <FormGroup row>
      <Label xs={6} sm={7} for={name}>
        {label}
      </Label>
      <Col xs={6} sm={5}>
        <Input
          required
          {...props}
          name={name}
          value={value}
          onChange={onChange}
        />
      </Col>
    </FormGroup>
  )
}

function NumericInput({
  label,
  name,
  model,
  setModel,
  ...props
}: BaseInputProps) {
  return (
    <BaseInput
      type='number'
      min={0}
      max={1000}
      step={1}
      {...props}
      model={model}
      name={name}
      label={label}
      setModel={setModel}
    />
  )
}

function TextInput({ label, name, model, setModel, ...props }: BaseInputProps) {
  return (
    <BaseInput
      type='text'
      {...props}
      model={model}
      name={name}
      label={label}
      setModel={setModel}
    />
  )
}

const INITIAL_PARAMS: Params = {
  start: 1,
  count: 200,
  prefix: '010101',
  width: 200,
  padding: 16,
  countPerRow: 20,
  copies: 1,
  topLabelSize: 20,
  topLabelHeight: 40,
  topLabelColor: '#FFFFFF',
  topLabelBackgroundColor: '#5EAF50',
  bottomLabelSize: 40,
  bottomLabelHeight: 60,
  bottomLabelColor: '#FFFFFF',
  bottomLabelBackgroundColor: '#5EAF50',
}

const NUMERIC_PARAMS: Array<keyof NumericParams> = [
  'start',
  'count',
  'width',
  'padding',
  'countPerRow',
  'copies',
  'topLabelSize',
  'topLabelHeight',
  'bottomLabelSize',
  'bottomLabelHeight',
]

const POSITIVE_NUMERIC_PARAMS: Array<keyof NumericParams> = [
  'start',
  'count',
  'width',
  'padding',
  'countPerRow',
  'copies',
]

const NON_NEGATIVE_NUMERIC_PARAMS: Array<keyof NumericParams> = [
  'topLabelSize',
  'topLabelHeight',
  'bottomLabelSize',
  'bottomLabelHeight',
]

const NON_EMPTY_PARAMS: Array<keyof StringParams> = [
  'prefix',
  'topLabelColor',
  'topLabelBackgroundColor',
  'bottomLabelColor',
  'bottomLabelBackgroundColor',
]

type ParamsFormProps = {
  onSubmit(params: Params | ((params: Params) => Params)): void
  saveToSvg(): void
}

function useParamsImport(setParams: Dispatch<SetStateAction<Params>>) {
  const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
    noClick: true,
    accept: { 'application/json': [] },
    multiple: false,
  })

  useEffect(() => {
    console.log(acceptedFiles)
    if (acceptedFiles.length !== 1) return
    const reader = new FileReader()
    reader.onabort = () => console.log('file reading was aborted')
    reader.onerror = () => console.log('file reading has failed')
    reader.onload = () =>
      active &&
      setParams(params => ({
        ...params,
        ...JSON.parse(reader.result?.toString() || '{}')?.params,
      }))
    reader.readAsText(acceptedFiles[0], 'utf-8')

    let active = true
    return () => {
      active = false
    }
  }, [acceptedFiles, setParams])

  return { getRootProps, getInputProps, open }
}

function ParamsForm({ onSubmit, saveToSvg }: ParamsFormProps) {
  const [params, setParams] = useState(INITIAL_PARAMS)
  const [error, setError] = useState('')
  const submit: FormEventHandler<HTMLFormElement> = useCallback(
    e => {
      e.preventDefault()
      const valid =
        NUMERIC_PARAMS.every(key => isFinite(+params[key])) &&
        POSITIVE_NUMERIC_PARAMS.every(key => +params[key] > 0) &&
        NON_NEGATIVE_NUMERIC_PARAMS.every(key => +params[key] >= 0) &&
        NON_EMPTY_PARAMS.every(key => params[key].length > 0)
      if (valid) {
        onSubmit(params)
        setError('')
      } else {
        setError('Проверьте параметры')
      }
    },
    [params, onSubmit],
  )

  const { getRootProps, getInputProps, open } = useParamsImport(setParams)
  const saveParamsToFile = useCallback(() => saveAsJson({ params }), [params])

  return (
    <Form onSubmit={submit}>
      <Row>
        <Col xs={12} md={6} lg={4}>
          <Label className='text-muted'>Параметры</Label>
          <NumericInput
            model={params}
            name='start'
            label='Нач-е знач-е'
            setModel={setParams}
            max={9999}
          />
          <NumericInput
            model={params}
            name='count'
            label='Количество'
            setModel={setParams}
          />
          <TextInput
            model={params}
            name='prefix'
            label='Префикс'
            setModel={setParams}
            maxLength={8}
          />
          <NumericInput
            model={params}
            name='width'
            label='Ширина элемента'
            setModel={setParams}
          />
          <NumericInput
            model={params}
            name='padding'
            label='Отступ'
            setModel={setParams}
            max={params.width}
          />
          <NumericInput
            model={params}
            name='countPerRow'
            label='Кол-о в ряде'
            setModel={setParams}
          />
          <NumericInput
            model={params}
            name='copies'
            label='Копии'
            setModel={setParams}
          />
        </Col>
        <Col xs={12} md={6} lg={4}>
          <Label className='text-muted'>Верхняя строка значения</Label>
          <NumericInput
            model={params}
            name='topLabelSize'
            label='Высота текста'
            setModel={setParams}
            max={9999}
          />
          <NumericInput
            model={params}
            name='topLabelHeight'
            label='Высота элемента'
            setModel={setParams}
          />
          <TextInput
            model={params}
            name='topLabelColor'
            label='Цвет текста'
            setModel={setParams}
          />
          <TextInput
            model={params}
            name='topLabelBackgroundColor'
            label='Цвет фона'
            setModel={setParams}
          />
        </Col>
        <Col xs={12} md={6} lg={4}>
          <Label className='text-muted'>Нижняя строка значения</Label>
          <NumericInput
            model={params}
            name='bottomLabelSize'
            label='Высота текста'
            setModel={setParams}
            max={9999}
          />
          <NumericInput
            model={params}
            name='bottomLabelHeight'
            label='Высота элемента'
            setModel={setParams}
          />
          <TextInput
            model={params}
            name='bottomLabelColor'
            label='Цвет текста'
            setModel={setParams}
          />
          <TextInput
            model={params}
            name='bottomLabelBackgroundColor'
            label='Цвет фона'
            setModel={setParams}
          />
        </Col>
      </Row>
      <Button type='submit' color='primary' className='me-2'>
        Применить
      </Button>
      <div className='d-inline me-2' {...getRootProps()}>
        <input {...getInputProps()} />
        <Button type='button' onClick={open} color='secondary'>
          Иморт параметров
        </Button>
      </div>
      <Button
        type='button'
        onClick={saveParamsToFile}
        color='secondary'
        className='me-2'
      >
        Экспорт параметров
      </Button>
      <Button type='button' onClick={saveToSvg} color='success'>
        Сохранить как файл
      </Button>
      {error && <Label className='text-danger ms-2'>{error}</Label>}
    </Form>
  )
}

type QrRectProps = {
  prefix: string
  code: string
  y: number
  width: number
  padding: number
}

function QrRect(props: QrRectProps) {
  const { prefix, code, y: top, width, padding } = props
  const [data, setData] = useState('')

  useEffect(() => {
    const qr = QR(0, 'M')
    qr.addData(`${prefix}${code}`, 'Alphanumeric')
    qr.make()
    const count = qr.getModuleCount()
    const size = (width - padding * 2) / count
    const data = Array.from(new Array(count * count))
      .map((_, i) => {
        const x = Math.floor(i / count) * size + padding
        const y = (i % count) * size + top + padding
        return qr.isDark(Math.floor(i / count), i % count)
          ? [
              `M ${x},${y}`,
              `L ${x + size},${y}`,
              `L ${x + size},${y + size}`,
              `L ${x},${y + size}`,
              `Z`,
            ].join(' ')
          : ''
      })
      .filter(Boolean)
      .join('\n')
    setData(data)
  }, [prefix, code, width, top, padding])

  return <path fill={'#000'} stroke='none' d={data} />
}

type SvgLabelProps = {
  label: string
  x: number
  y: number
  width: number
  height: number
  size: number
  color: string
  backgroundColor: string
}

function SvgLabel(props: SvgLabelProps) {
  const { label, x, y, width, height, size, color, backgroundColor } = props
  return (
    <>
      <rect x={x} y={y} width={width} height={height} fill={backgroundColor} />
      <text
        textAnchor='middle'
        dominantBaseline='central'
        x={(width / 2).toFixed()}
        y={(y + height / 2).toFixed()}
        fill={color || '#212121'}
        style={{ fontSize: size }}
      >
        {label}
      </text>
    </>
  )
}

function useTopLabel(props: Params): Omit<SvgLabelProps, 'label'> {
  const {
    width,
    topLabelHeight,
    topLabelSize,
    topLabelColor,
    topLabelBackgroundColor,
  } = props
  return useMemo(() => {
    return {
      x: 0,
      y: 0,
      width: +width,
      height: +topLabelHeight,
      size: +topLabelSize,
      color: topLabelColor,
      backgroundColor: topLabelBackgroundColor,
    }
  }, [
    width,
    topLabelHeight,
    topLabelSize,
    topLabelColor,
    topLabelBackgroundColor,
  ])
}

function useBottomLabel(props: Params): Omit<SvgLabelProps, 'label'> {
  const {
    width,
    topLabelHeight,
    bottomLabelHeight,
    bottomLabelSize,
    bottomLabelColor,
    bottomLabelBackgroundColor,
  } = props
  return useMemo(() => {
    return {
      x: 0,
      y: +topLabelHeight + +width,
      width: +width,
      height: +bottomLabelHeight,
      size: +bottomLabelSize,
      color: bottomLabelColor,
      backgroundColor: bottomLabelBackgroundColor,
    }
  }, [
    width,
    topLabelHeight,
    bottomLabelHeight,
    bottomLabelSize,
    bottomLabelColor,
    bottomLabelBackgroundColor,
  ])
}

type QrCodeItemProps = {
  x: number
  y: number
  width: number
  height: number
  topProps: Omit<SvgLabelProps, 'label'>
  bottomProps: Omit<SvgLabelProps, 'label'>
  prefix: string
  code: string
  padding: number
}

function QrCodeItem(props: QrCodeItemProps) {
  const { x, y, width, height, topProps, bottomProps, prefix, code, padding } =
    props
  const id = `qr-code-${code}`
  return (
    <>
      <symbol
        id={id}
        width={width}
        height={height}
        viewBox={`0 0 ${width} ${height}`}
      >
        {topProps.height > 0 && (
          <SvgLabel {...topProps} label={`${prefix}${code}`} />
        )}
        {bottomProps.height > 0 && <SvgLabel {...bottomProps} label={code} />}
        <QrRect
          prefix={prefix}
          code={code}
          y={topProps.height}
          width={+width}
          padding={+padding}
        />
        <rect
          x='0'
          y='0'
          width={width}
          height={height}
          fill={'rgba(0,0,0,0)'}
          strokeWidth={1}
          stroke='#777'
        />
      </symbol>
      <use xlinkHref={`#${id}`} x={x} y={y} />
    </>
  )
}

function QrCodeGenerator(props: Params & { svgRef: LegacyRef<SVGSVGElement> }) {
  const {
    width: itemWidth,
    topLabelHeight,
    bottomLabelHeight,
    count,
    countPerRow,
    copies,
    start,
    prefix,
    padding,
    svgRef,
  } = props
  const itemHeight = +topLabelHeight + +itemWidth + +bottomLabelHeight
  const width = +itemWidth * +countPerRow * (+copies > 1 ? 2 : 1)
  const height =
    itemHeight * Math.ceil(+count / +countPerRow) * Math.ceil(+copies / 2)
  const topProps = useTopLabel(props)
  const bottomProps = useBottomLabel(props)
  const codes = useMemo(() => {
    let codes = Array.from(new Array(+count))
      .map((_, i) => `${+start + i}`.padStart(3, '0'))
      .reduce(
        (codes_list, code) => {
          const codes = codes_list[codes_list.length - 1]
          if (codes.length < +countPerRow) codes.push(code)
          else codes_list.push([code])
          return codes_list
        },
        [[]] as string[][],
      )
    if (+copies > 1) codes = codes.map(cc => [...cc, ...cc])
    if (+copies > 2)
      codes = new Array<string[][]>(Math.ceil(+copies / 2)).fill(codes).flat(1)
    return codes
  }, [count, start, countPerRow, copies])
  return (
    <svg
      ref={svgRef}
      version='1.1'
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'
      preserveAspectRatio='xMinYMin meet'
      viewBox={`0 0 ${width} ${height}`}
      width={width}
      height={height}
      fill={'#fff'}
    >
      {codes.map((cc, i) =>
        cc.map((code, j) => {
          const x = +itemWidth * j
          const y = +itemHeight * i
          return (
            <QrCodeItem
              key={`${code}-${i}-${j}`}
              prefix={prefix}
              code={code}
              x={x}
              y={y}
              width={+itemWidth}
              height={+itemHeight}
              topProps={topProps}
              bottomProps={bottomProps}
              padding={+padding}
            />
          )
        }),
      )}
    </svg>
  )
}

function App() {
  const [params, setParams] = useState(INITIAL_PARAMS)
  const svgRef = useRef<SVGSVGElement>(null)
  const saveToSvg = useCallback(
    () => svgRef.current && saveAsSvg(svgRef.current.outerHTML),
    [svgRef],
  )
  return (
    <>
      <Container fluid className='mb-3'>
        <ParamsForm onSubmit={setParams} saveToSvg={saveToSvg} />
      </Container>
      <QrCodeGenerator {...params} svgRef={svgRef} />
    </>
  )
}

export default App
