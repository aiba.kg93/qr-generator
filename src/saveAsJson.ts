import FileSaver from 'file-saver'

export default function saveAsJson(data: Record<string, any>) {
  const blob = new Blob([JSON.stringify(data)], {
    type: 'application/json;charset=utf-8',
  })
  FileSaver.saveAs(blob, 'qr-code-params.json')
}
